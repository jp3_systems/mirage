@echo off

:JavaCheck
javaw -version >nul 2>&1 && (
    goto CommonsCheck
) || (
    echo Java is missing from this computer and is needed to run Mirage. Mirage will now exit.
    pause
	goto Exit
)

:CommonsCheck
if exist lib\commons-io-2.5.jar (
    goto Run
) else (
    echo lib\commons-io-2.5.jar is missing from this computer and is needed to run Mirage. Please download Mirage again. Mirage will now exit.
    pause
	goto Exit
)


:Run
start javaw -jar lib\mirage.jar -g

:Exit
exit

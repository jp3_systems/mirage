#!/bin/bash

java -version > /dev/null 2>&1
check1=$(echo $?)
if [[ ${check1} -ne 0 ]]; then
	echo $(date) >> error.log
	echo "Java is missing from this computer and is needed to run Mirage. Mirage will now exit." | tee -a error.log
	exit
fi

ls lib/commons-io-2.5.jarx > /dev/null 2>&1
check2=$(echo $?)
if [[ ${check2} -ne 0 ]]; then
	echo $(date) >> error.log
	echo "lib\commons-io-2.5.jar is missing from this computer and is needed to run Mirage. Please download Mirage again. Mirage will now exit." | tee -a error.log
	exit
fi

java -jar lib/mirage.jar -g

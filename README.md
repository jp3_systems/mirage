<h1>Mirage</h1>
<hl/>
Named after the X-Men's Mirage, this is a simple Java application intended to be used for a picture frame. Where Mirage the character would generally generate fearful images, this Mirage should ideally be used to display your pleasant images onto your DIY digital picture frame.<br/><br/>
Yes, it's a stripped down screensaver, but there would be no fun in a DIY project that way :). It wasn't until I was halfway through that I realized what I was doing. But, it's basically my Hello World project, so pfft. I tried to incorporate as many things as I could, like this read me file, to get used to the many things that come with development projects.<br/>
<h3>Requirements</h3>
Java 7 or better (just need JRE)<br/>
Tested on:<br/>
Windows 10 (though I do not see any reason why it wouldn't run on most versions of Windows)<br/>
Ubuntu 16.10 (though I do not see any reason why it wouldn't run on most versions of Linux)<br/>
OSX 10.12.6 Sierra (though I do not see any reason why it wouldn't run on most versions of OSX)<br/>
<h3>Installation</h3>
Installation is simple. Download the Mirage-x.x.xx folder and place it in the location of choice on your target machine. Done.<br/>
A configuration file is in the main directory and named Mirage.conf.xml.<br/>
You can also download the whole project and run the Ant build process yourself.<br/>
<h3>Usage</h3>
Run one of the start scripts. OSX should use the Linux script.<br/>
The application is self contained and comes with a set of sample images. You can use this default directory or change it to another folder of your choosing.<br/>
The delay between images setting is the amount of minutes that each image should remain on the screen. This must be a whole number. Default is 5 minutes.<br/>
The fade in option controls whether the image transitions use a fade in effect or simply change. Options are Y or N. Default is Y.<br/>
Command line:  start javaw -jar lib\mirage.jar -g<br/>
Options:<br/>
-g :  Runs Mirage with the GUI. This is what the scripts execute.<br/>
-gd:  Runs Mirage with the GUI in "debug" mode. Difference here is that it will ignore the delay setting and cycle through the images at 5 second intervals.<br/>
-s :  Runs Mirage settings menu in command line mode.<br/>
-d :  Runs Mirage from the command line in debug mode.<br/>
No option added will run Mirage off of the command line.<br/>
<h3>Known Issues</h3>
If your frame is oriented in landscape, protrait images will be stretched to fit. Similarly, if you have the frame in portrait orientation, landscape images will be stretched. Eventually I will correct for this.<br/>
<h3>License</h3>
commons-io-2.5.jar is used thanks to the fine folks at Apache, using whatever license they use.
Sample images are from Wikimedia:<br/>
aurora: <a href="https://commons.wikimedia.org/wiki/File:Aurora_borealis_above_Lyngenfjorden,_2012_March.jpg">https://commons.wikimedia.org/wiki/File:Aurora_borealis_above_Lyngenfjorden,_2012_March.jpg</a><br/>
milky way: <a href="https://commons.wikimedia.org/wiki/File:Arco_da_Via_L%C3%A1ctea_sobre_Corumb%C3%A1.jpg">https://commons.wikimedia.org/wiki/File:Arco_da_Via_L%C3%A1ctea_sobre_Corumb%C3%A1.jpg</a><br/>
nyc: <a href="http://architectureimg.com/new-york-arial-large-cities-usa-city-hd-background/">http://architectureimg.com/new-york-arial-large-cities-usa-city-hd-background/</a><br/>
Special thanks to the wonderful users of Stack Overflow for answering other people's questions which in turn gave me the help I needed. <br/>
For all of my code... I've no clue about licenses. As far as I'm concerned, my work is all free as in beer for everyone. Have at it!<br/>
If you find this useful, I would love to hear about it! <a href="mailto:joe@jp3.systems?Subject=Mirage%20is%20Awesome" target="_blank">joe@jp3.systems</a> <br/>



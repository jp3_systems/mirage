package systems.jp3.mirage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;
import java.util.Random;

public class RunFrame {
	static float alpha = 0f;
	int i;
	int delay;
	static String closeFrame = "N";
	public void runFrame(char gui, char debug) throws IOException, SAXException {
		ReadProperties rp = new ReadProperties();
		rp.propertiesRead();
		if (ValidateProperties.isFade(String.valueOf(rp.fadeIn)) && ValidateProperties.isDelay(String.valueOf(rp.delay)) && ValidateProperties.isPath(rp.picPath)) {
			if (debug == 'N') {
				delay = rp.delay*60*1000;
			} else {
				delay = 5000;
			}
			File f = new File(rp.picPath);
			List<String> names = new ArrayList<String>(Arrays.asList(f.list()));
			Iterator<String> iterator = names.iterator();
			while(iterator.hasNext()) {
				String j = iterator.next();
				if ( ! Arrays.asList( "jpg","gif","png" ).contains( FilenameUtils.getExtension(j).toLowerCase() ) ) {
					iterator.remove();
				}
			}
			DrawFrame d = new DrawFrame();
			d.FrameDraw(rp.picPath + names.get(0), rp.fadeIn, gui);
			try {Thread.sleep(delay);} catch (InterruptedException e) {e.printStackTrace();}
			Random rand = new Random();
			long expectedtime = System.currentTimeMillis();
			while (RunFrame.closeFrame.equals("N")) {
				i = rand.nextInt(names.size());
				d.filename1 = rp.picPath + names.get(i);
				d.alpha = 0.75f;
	        	d.frame.repaint();
				while(System.currentTimeMillis() < expectedtime){}
				expectedtime += delay;
			}
		}
	}
}

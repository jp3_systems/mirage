package systems.jp3.mirage;
import javax.swing.*;
import org.xml.sax.SAXException;
import systems.jp3.mirage.Mirage.globalVar;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class MirageSettingsGUI extends Frame {
	private static final long serialVersionUID = 1L;
	//Declare variables
	static JFrame frame1;
	static Container pane;
	static JButton btnSave, btnExit, btnPath, btnReset;
	static JLabel lblPath, lblDelay, lblFade;
	static JTextField txtPath, txtDelay, txtFade;
	static Insets insets;

	public static void mirageSettingsUI () throws SAXException, IOException{
		//Set Look and Feel
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		catch (ClassNotFoundException e) {}
		catch (InstantiationException e) {}
		catch (IllegalAccessException e) {}
		catch (UnsupportedLookAndFeelException e) {}

		//Create the frame
		frame1 = new JFrame ("Mirage Settings");
		frame1.setSize (500, 200);
		pane = frame1.getContentPane();
		insets = pane.getInsets();
		pane.setLayout (null);
		
	    frame1.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
	    frame1.getRootPane().getActionMap().put("Cancel", new AbstractAction() {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {frame1.dispose();}
	    });

	    MirageImg mi = new MirageImg();
	    mi.logo.setBounds(frame1.getWidth() - mi.logo.getPreferredSize().width - 20, frame1.getHeight() - 45 -  mi.logo.getPreferredSize().height , mi.logo.getPreferredSize().width, mi.logo.getPreferredSize().height);
		frame1.add(mi.logo);
		frame1.setIconImage(mi.icon.getImage());
		frame1.getContentPane().setBackground(Color.white);

		ReadProperties rp = new ReadProperties();
		rp.propertiesRead();

		//Create controls
		btnSave = new JButton ("Save");
		btnExit = new JButton ("Exit");
		btnReset = new JButton ("Reset Defaults");
		btnPath = new JButton ("...");
		lblPath = new JLabel ("Picture Path:");
		lblDelay = new JLabel ("Delay (in minutes):");
		lblFade = new JLabel ("Fade In (Y/N):");
		txtPath = new JTextField (rp.picPath,35);
		txtDelay = new JTextField (String.valueOf(rp.delay),10);
		txtFade = new JTextField (String.valueOf(rp.fadeIn),10);

		//Add all components to panel
		pane.add (btnPath);
		pane.add (lblPath);
		pane.add (lblDelay);
		pane.add (lblFade);
		pane.add (txtPath);
		pane.add (txtDelay);
		pane.add (txtFade);
		pane.add (btnSave);
		pane.add (btnExit);
		pane.add (btnReset);
		
		//Place all components
		lblPath.setBounds (insets.left + 5, insets.top + 5, lblPath.getPreferredSize().width, lblPath.getPreferredSize().height);
		txtPath.setBounds (100, insets.top + 5, txtPath.getPreferredSize().width, txtPath.getPreferredSize().height);
		lblDelay.setBounds (insets.left + 5, txtPath.getY() + txtPath.getHeight() + 5, lblDelay.getPreferredSize().width, lblDelay.getPreferredSize().height);
		txtDelay.setBounds (100, txtPath.getY() + txtPath.getHeight() + 5, txtDelay.getPreferredSize().width, txtDelay.getPreferredSize().height);
		lblFade.setBounds (insets.left + 5, txtDelay.getY() + txtDelay.getHeight() + 5, lblFade.getPreferredSize().width, lblFade.getPreferredSize().height);
		txtFade.setBounds (100, txtDelay.getY() + txtDelay.getHeight() + 5, txtFade.getPreferredSize().width, txtFade.getPreferredSize().height);
		btnSave.setBounds (insets.left + 25, txtFade.getY() + txtFade.getHeight() + 15, btnSave.getPreferredSize().width, btnSave.getPreferredSize().height);
		btnExit.setBounds (btnSave.getX() + btnSave.getWidth() + 15, btnSave.getY(), btnExit.getPreferredSize().width, btnExit.getPreferredSize().height);
		btnPath.setBounds (txtPath.getX() + txtPath.getWidth() + 2, insets.top + 4, btnPath.getPreferredSize().width, btnPath.getPreferredSize().height -1);
		btnReset.setBounds (frame1.getWidth() - btnReset.getPreferredSize().width - 25, btnSave.getY(), btnReset.getPreferredSize().width, btnReset.getPreferredSize().height);

		btnReset.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		//Set frame visible
		frame1.setLocationRelativeTo(null);
		frame1.setVisible (true);

		//Button's action
		btnSave.addActionListener(new btnSaveAction());
		btnExit.addActionListener(new btnExitAction());
		btnPath.addActionListener(new btnPathAction());
		btnReset.addActionListener(new btnResetAction());

	}

	public static class btnSaveAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			SaveProperties sp = new SaveProperties();
			try {
				if (ValidateProperties.isFade(txtFade.getText()) && ValidateProperties.isDelay(txtDelay.getText()) && ValidateProperties.isPath(txtPath.getText())) {
					sp.propertiesSave(txtPath.getText(), Integer.parseInt(txtDelay.getText()), txtFade.getText().charAt(0));
					JOptionPane.showMessageDialog(frame1, "Settings Saved!");
				} else {
					JOptionPane.showMessageDialog(frame1, "Save Failed!");
				}
			} catch (SAXException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public static class btnExitAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			frame1.dispose();
		}
	}
	
	public static class btnPathAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			try {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
			fc.showSaveDialog(null);
			File file = fc.getSelectedFile();
			txtPath.setText(file.getAbsolutePath());
			} catch (NullPointerException e1) {
				//do nothing!
			}
		}
	}

	public static class btnResetAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			SaveProperties sp = new SaveProperties();
			try {
				txtPath.setText(globalVar.dPicPath);
				txtDelay.setText(String.valueOf(globalVar.dDelay));
				txtFade.setText(String.valueOf(globalVar.dFadeIn));
				if (ValidateProperties.isFade(txtFade.getText()) && ValidateProperties.isDelay(txtDelay.getText()) && ValidateProperties.isPath(txtPath.getText())) {
					sp.propertiesSave(txtPath.getText(), Integer.parseInt(txtDelay.getText()), txtFade.getText().charAt(0));
					JOptionPane.showMessageDialog(frame1, "Settings Saved!");
				} else {
					JOptionPane.showMessageDialog(frame1, "Save Failed!");
				}
			} catch (SAXException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
}

package systems.jp3.mirage;
import java.io.IOException;
import java.util.Scanner;
import org.xml.sax.SAXException;
import systems.jp3.mirage.Mirage.globalVar;

public class SettingsMenu {
	public void MenuSettings() throws SAXException, IOException {
		ReadProperties rp = new ReadProperties();
		rp.propertiesRead();
		Scanner s = new Scanner(System.in);
		int x = 0, y = 0;
		while (x == 0) {
			try{
				System.out.println("\nSetting options:");
				System.out.println("1. Path to images (include trailing slash) (current: " + rp.picPath + ")");
				System.out.println("2. Time between images in minutes (current: " + rp.delay + ")");
				System.out.println("3. Fade in images (current: " + rp.fadeIn + ")");
				System.out.println("8. Reset to defaults");
				System.out.println("9. Save and Exit");
				System.out.println("0. Exit without Save");
				System.out.println("\nEnter option:");
				int o = s.nextInt();
				if (o == 1) {
					System.out.println("Enter new path:");
					String tpath = s.next();
					if (ValidateProperties.isPath(tpath)) {
						rp.picPath = tpath;
					} else {
						System.in.read();
					}
				} else if (o == 2) {
					System.out.println("Enter time between images (in minutes):");
					String tdelay = s.next();
					if ( ValidateProperties.isDelay(tdelay) ) {
						rp.delay = Integer.parseInt(tdelay);
					} else {
						System.in.read();
					}
				} else if (o == 3) {
					System.out.println("Fade in images Y/N:");
					char tfade = s.next().charAt(0);
					if ( ValidateProperties.isFade(String.valueOf(tfade)) ) {
						rp.fadeIn = tfade;
					} else {
						System.in.read();
					}
				} else if (o == 8) {
					rp.picPath = globalVar.dPicPath;
					rp.delay = globalVar.dDelay;
					rp.fadeIn = globalVar.dFadeIn;
				} else if (o == 9) {
					y = 1;
					break;
				} else if (o == 0) {
					break;
				} else {
					break;
				}
			} catch (Exception err) {
				System.out.println("Error: " + err.getMessage());
				System.exit(1);
			}
		}
		s.close();
		if (y == 1) {
			SaveProperties sp = new SaveProperties();
			sp.propertiesSave(rp.picPath, rp.delay, rp.fadeIn);
			System.out.println("Changes have been saved to " + globalVar.confFile);
			System.exit(0);
		} else {
			System.out.println("No changes saved.");
			System.exit(0);
		}
	}
}

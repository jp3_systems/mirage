package systems.jp3.mirage;
import java.io.File;
import java.io.IOException;
import org.xml.sax.SAXException;

public class Mirage {

	public static void main(String[] args) throws SAXException, IOException {
		
		try{Thread.sleep(1500); } catch(Exception e) {}
		
		if (args.length > 0 && args[0].equals("-s")) {
			SettingsMenu sm = new SettingsMenu();
			sm.MenuSettings();
		} else if (args.length > 0 && args[0].equals("-d")) {
			RunFrame x = new RunFrame();
			try {
				x.runFrame('N', 'Y');
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("No pictures found in the currently selected folder.\nAdd pictures or change the path in Settings.");
			}
		} else if (args.length > 0 && args[0].equals("-g")) {
			MirageGUI.mirageUI('N');
		} else if (args.length > 0 && args[0].equals("-gd")) {
			MirageGUI.mirageUI('Y');
		} else {
			RunFrame x = new RunFrame();
			try {
				x.runFrame('N', 'N');
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("No pictures found in the currently selected folder.\nAdd pictures or change the path in Settings.");
			}
		}
	}

	public static class globalVar {
		static File f = new File("pics");
		static String p = f.getAbsolutePath().toString();
	    public static final String confFile="Mirage.conf.xml";
	    public static final String dPicPath = p;
	    public static final int dDelay = 5;
	    public static final char dFadeIn = 'Y';
	}
}

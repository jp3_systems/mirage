package systems.jp3.mirage;

import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class MirageImg {
	public JLabel logo;
	public ImageIcon icon;
	
	public MirageImg () {

		URL logoURL = this.getClass().getResource("/systems/jp3/mirage/jp3logo.png");
		ImageIcon img = new ImageIcon(logoURL);
		Image simg = img.getImage().getScaledInstance(20, 10, Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(simg);
		logo = new JLabel(image);
	
		URL iconURL = this.getClass().getResource("/systems/jp3/mirage/mirageicon.png");
		icon = new ImageIcon(iconURL);
	
	}

}

package systems.jp3.mirage;
import javax.swing.*;
import org.xml.sax.SAXException;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
/**
 * MirageGUI is the main UI class for Mirage. This is called by running via start script or using the command line with -g option.
 * <p>The UI provides 3 buttons:</p>
 * <ul>
 * <li>Run Mirage</li>
 * <li>Settings</li>
 * <li>Exit</li>
 * </ul>
 * 
 * @author Joe
 */
public class MirageGUI extends Frame {
	private static final long serialVersionUID = 1L;
	static JFrame frame1;
	static Container pane;
	static JButton btnRun, btnSettings, btnQuit;
	static Insets insets;
	static JTextField txtDebug;

	public static void mirageUI (char debug) throws IOException{
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		catch (ClassNotFoundException e) {}
		catch (InstantiationException e) {}
		catch (IllegalAccessException e) {}
		catch (UnsupportedLookAndFeelException e) {}
		
		txtDebug = new JTextField (String.valueOf(debug),1);

		/**Create JFrame*/
		frame1 = new JFrame ("Mirage");
		frame1.setSize (250, 250);
		pane = frame1.getContentPane();
		insets = pane.getInsets();
		pane.setLayout (null);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.getContentPane().setBackground(Color.black);

		/**Generate the logo and icon and add it to the JFrame. */
	    MirageImg mi = new MirageImg();
	    mi.logo.setBounds(frame1.getWidth() - mi.logo.getPreferredSize().width - 20, frame1.getHeight() - 45 -  mi.logo.getPreferredSize().height , mi.logo.getPreferredSize().width, mi.logo.getPreferredSize().height);
		frame1.add(mi.logo);
		frame1.setIconImage(mi.icon.getImage());

		/**Declare the buttons, add them to the container and set their size and location.*/
		btnRun = new JButton ("Launch Mirage");
		btnSettings = new JButton ("Settings");
		btnQuit = new JButton ("Exit");

		pane.add (btnRun);
		pane.add (btnSettings);
		pane.add (btnQuit);

		btnRun.setBounds (insets.left + 45, insets.top + 5, 150, 50);
		btnSettings.setBounds (insets.left + 45, btnRun.getY() + btnRun.getHeight() + 5, 150, 50);
		btnQuit.setBounds (insets.left + 45, btnSettings.getY() + btnSettings.getHeight() + 5, 150, 50);
		
		frame1.setLocationRelativeTo(null);
		frame1.setVisible(true);

		/**Set button actions.*/
		btnRun.addActionListener(new btnRunAction()); 
		btnSettings.addActionListener(new btnSettingsAction()); 
		btnQuit.addActionListener(new btnQuitAction()); 

	}
	
	/**<p>btnRunAction implements the action listener for the Run Mirage button. This initiates a new RunFrame object.</p>
	 * <p>If no images are in the specified location, the user will be prompted to add some.</p>
	 */
	public static class btnRunAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			Thread one = new Thread() {
			    public void run() {
			RunFrame x = new RunFrame();
				try {
					x.runFrame('Y',txtDebug.getText().charAt(0));
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (SAXException e1) {
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(frame1, "No pictures found in the currently selected folder.\nAdd pictures or change the path in Settings.");
				}
			}
			};
			one.start();
		}
	}

	/**btnSettingsAction implements the action listener for the Settings button.  This initiates a new MirageSettingsGUI object.*/
	public static class btnSettingsAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			try {
				MirageSettingsGUI.mirageSettingsUI();
			} catch (SAXException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**btnQuitAction implemets the action listener for the Exit button.  This will exit the application.*/
	public static class btnQuitAction implements ActionListener{
		public void actionPerformed (ActionEvent e){
			System.exit(0);
		}
	}

}
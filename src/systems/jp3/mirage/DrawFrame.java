package systems.jp3.mirage;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class DrawFrame extends RunFrame{
	float alpha = 0.75f;
	public String filename;
	public String filename1;
	public JFrame frame;
	public void FrameDraw(String x, char y, char z) {
		filename1 = x; 
		filename = x;
		frame = new JFrame("Mirage");
	    frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel"); //$NON-NLS-1$
	    frame.getRootPane().getActionMap().put("Cancel", new AbstractAction() {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				if (z == 'N') {
					System.exit(0);
				} else if (z == 'Y') {
					RunFrame.closeFrame = "Y";
					frame.dispose();
				}
	        }
	    });
	    frame.setUndecorated(true);
	    MirageImg mi = new MirageImg();
		frame.setIconImage(mi.icon.getImage());

	    try {
			frame.add(new JComponent() {
				private static final long serialVersionUID = 1L;
				BufferedImage img = ImageIO.read(new File(filename));
			    public void paintComponent(Graphics g) {
			        super.paintComponent(g);
			        Graphics2D g2d = (Graphics2D) g.create();
			        if ( y == 'Y') {
				        g2d.setComposite(AlphaComposite.SrcOver.derive(alpha));
				        alpha += 0.01f;
				        if (alpha >= 1.0f) {
				            alpha = 1.0f;
				        } else {
				            repaint();
				        }
			        }
			        g2d.drawImage(img, 0, 0, getWidth(), getHeight(), this);
				    if (! filename.equals(filename1)) {
						try {
							img = ImageIO.read(new File(filename1));
							repaint();
						} catch (IOException e) {
							e.printStackTrace();
						}
						filename = filename1;
					} 
			    }
			});
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    GraphicsDevice gs = ge.getDefaultScreenDevice();
	    gs.setFullScreenWindow(frame);
	    frame.validate();
	}
}
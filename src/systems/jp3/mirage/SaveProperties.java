package systems.jp3.mirage;
import org.xml.sax.SAXException;

import systems.jp3.mirage.Mirage.globalVar;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class SaveProperties {
	public void propertiesSave(String sPicPath, int sDelay, char sFadeIn) throws SAXException, IOException {
        String w;
        w = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
            	+"<jp3Frame xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"Mirage.conf.xsd\">\n"
            	+"	<property>\n"
        		+"		<!--picPath: Must be a valid path-->\n"
            	+"		<picPath>" + sPicPath + "</picPath>\n"
        		+"		<!--delay: Must be a whole number-->\n"
            	+"		<delay>" + sDelay + "</delay>\n"
        		+"		<!--fadeIn: Must be a Y or N-->\n"
            	+"		<fadeIn>" + sFadeIn + "</fadeIn>\n"
            	+"	</property>\n"
            	+"</jp3Frame>\n";

        try {
        	BufferedWriter bw = new BufferedWriter(new FileWriter(globalVar.confFile));
        	bw.write(w);
        	bw.close();
			//System.out.println(w);
		} catch (FileNotFoundException err) { 
			System.out.println("file not found.");
			System.exit(1);
		} catch (IOException err) {
		    System.err.format("IOException: %s%n", err);
		    System.exit(1);
		} catch (Exception err3) {
			System.out.println("Error: " + err3.getMessage());
			System.exit(1);
		}
	}
}

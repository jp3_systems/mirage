package systems.jp3.mirage;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import systems.jp3.mirage.Mirage.globalVar;

public class ReadProperties {
	String picPath;
	int delay;
	char fadeIn;

	public void propertiesRead () throws SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(globalVar.confFile);
			
			if (doc.getElementsByTagName("picPath").item(0) == null || doc.getElementsByTagName("picPath").item(0).getTextContent() == "") {
				System.out.println("Element <picPath> is missing or empty in config file.");
				picPath = globalVar.dPicPath;
			} else {
				picPath = doc.getElementsByTagName("picPath").item(0).getTextContent();
			}
			picPath = correctPath(picPath);

			if (doc.getElementsByTagName("delay").item(0) == null || doc.getElementsByTagName("delay").item(0).getTextContent() == "") {
				System.out.println("Element <delay> is missing or empty in config file.");
				delay = globalVar.dDelay;
			} else {
				delay = Integer.parseInt(doc.getElementsByTagName("delay").item(0).getTextContent());
			}
			
			if (doc.getElementsByTagName("fadeIn").item(0) == null || doc.getElementsByTagName("fadeIn").item(0).getTextContent() == "") {
				System.out.println("Element <fadeIn> is missing or empty in config file.");
				fadeIn = globalVar.dFadeIn;
			} else {
				fadeIn = doc.getElementsByTagName("fadeIn").item(0).getTextContent().charAt(0);
			}

		//System.out.println(picPath + " - " + delay + " - " + fadeIn);
			
		} catch (FileNotFoundException err) {
			SaveProperties sp = new SaveProperties();
			sp.propertiesSave(globalVar.dPicPath, globalVar.dDelay, globalVar.dFadeIn);
			System.out.println(globalVar.confFile + " was missing. A default file has been created.");
			propertiesRead();
		} catch (SAXParseException err) {
			SaveProperties sp = new SaveProperties();
			sp.propertiesSave(globalVar.dPicPath, globalVar.dDelay, globalVar.dFadeIn);
			System.out.println(globalVar.confFile + " was empty. A default file has been created.");
			propertiesRead();
		} catch (ParserConfigurationException err2) {
			System.out.println("ParserConfigurationException: Regenerate " + globalVar.confFile);
			System.exit(1);
		} catch (Exception err3) {
			System.out.println("Error: " + err3.getClass());
			System.exit(1);
		}
	}

	public static String correctPath(String x) {
		String OS = System.getProperty("os.name");
		if (OS.startsWith("Win")) {
			if (x.endsWith("\\")) {
				return x;
			} else {
				return x + "\\";
			}
		} else {
			if (x.endsWith("/")) {
				return x;
			} else {
				return x + "/";
			}
		}
	}

}

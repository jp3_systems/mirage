package systems.jp3.mirage;

import java.io.File;

import javax.swing.JOptionPane;

public class ValidateProperties {
	
	public static boolean isDelay (String x){
		try { 
			Integer.parseInt(x);
		} catch (NumberFormatException e1) {
			JOptionPane.showMessageDialog(null, "Invalid value entered for time between images.  Please enter only a valid integer.");
			System.out.println("Invalid value entered for time between images.  Please enter only a valid integer. \nPress any key to continue...");
			return false;
		}
		return true;
	}

	public static boolean isFade (String x){
		if ( ! (x.equals("Y")) && ! (x.equals("N")) ) {
			JOptionPane.showMessageDialog(null, "Invalid value entered for fade in images.  Please enter only Y or N.");
			System.out.println("Invalid value entered for fade in images.  Please enter only Y or N. \nPress any key to continue...");
			return false;
		}
		return true;
	}

	public static boolean isPath (String x){
		File p = new File(x);
		if ( ! p.exists() ) {
			JOptionPane.showMessageDialog(null, "Invalid value entered for path to images.  Please enter a valid path.");
			System.out.println("Invalid value entered for path to images.  Please enter a valid path. \nPress any key to continue...");
			return false;
		}
		return true;
	}

}
